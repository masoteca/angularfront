import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {Routing, appRoutingProviders} from './app.routing';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login.component';
import {RegisterComponent} from './components/register.component';
import {HomeComponent} from './components/home.component';
import {FormsModule} from '@angular/forms';
import {UserEditComponent} from './components/user.edit.component';
import {TaskNewComponent} from './components/task.new.component';
import {TaskDetailComponent} from './components/task.detail.component';
import {TaskEditComponent} from './components/task.edit.component';
import {generateDatePipe} from './pipes/generate.date.pipe';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        HomeComponent,
        UserEditComponent,
        TaskNewComponent,
        TaskDetailComponent,
        TaskEditComponent,
        generateDatePipe
    ],
    imports: [
        BrowserModule,
        Routing,
        FormsModule,
        HttpClientModule
    ],
    providers: [
        appRoutingProviders,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
