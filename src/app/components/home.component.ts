import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../services/user.service';
import {TaskService} from '../services/task.service';
import {Task} from '../models/task';

@Component({
    selector: 'home',
    templateUrl: '../views/home.html',
    providers: [UserService, TaskService]
})
export class HomeComponent implements OnInit {
    public title: string;
    public token;
    public status;
    public tasks: Array<Task>;
    public identity;
    public pages;
    public pagePrev;
    public pageNext;
    public loading: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _taskService: TaskService,
        private _userService: UserService
    ) {
        this.title = 'Componente de Home';
        this.token = _userService.getToken();
        this.identity = _userService.getIdentity();
    }

    ngOnInit() {
        this.getAllTask();
    }

    // genera lista de tareas por hacer.
    getAllTask() {
        this._route.params.forEach((params: Params) => {
            let page = +params['page'];

            if (!page) {
                page = 1;
            }
            this.loading = 'show';
            this._taskService.getTasks(this.token, page).subscribe(
                response => {
                    this.status = response.status;
                    if (response.status == 'succes') {
                        this.loading = 'hidden';
                        this.tasks = response.data;
                        this.pages = [];

                        // se recorre el total de pagina y se hace push
                        //al array para tener una paginacion con interfaz
                        //da el total de paginas
                        for (let i = 0; i < response.total_pages; i++) {
                            this.pages.push(i);
                        }

                        //pagina anterior
                        if (page >= 2) {
                            this.pagePrev = (page - 1);
                        } else {
                            this.pagePrev = page;
                        }
                        //pagina siguiente
                        if (page < response.total_pages || page == 1) {
                            this.pageNext = (page + 1);
                        } else {
                            this.pageNext = page;
                        }
                    }
                },
                error => {
                    console.log(<any>error);
                }
            );

        });
    }

    public filter = 0;
    public order = 0;
    public searchString;

    search() {
        this.loading = 'show';
        //aseguramos que valor defecto sea null en caso de ir vacio 
        if (!this.searchString || this.searchString.trim().length == 0) {
            this.searchString = null;
        }
        this._taskService.search(this.token, this.searchString, this.filter, this.order).subscribe(
            response => {
                if (response.status == 'succes') {
                    this.tasks = response.data;
                    this.loading = 'hidden';
                } else {
                    this._router.navigate(['/']);
                }
            },
            error => {
                console.log(<any>error);
            }
        );

    }
}

