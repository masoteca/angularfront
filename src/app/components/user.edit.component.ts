import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
    selector: 'user-edit',
    templateUrl: '../views/user_edit.html',
    providers: [UserService]
})
export class UserEditComponent implements OnInit{
    public title: string;
    public _user: User;
    public status;
    public identity;
    public token;

    constructor(
    private _UserService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
    ){
        this.title ="Modificar mis datos";
        this.identity = this._UserService.getIdentity();
        this.token = this._UserService.getToken();
    }

    ngOnInit(){
    if(this.identity == null){  
        this._router.navigate(['/login']);
    }else{
    this._user =new User(
        this.identity.sub,
        this.identity.role,
        this.identity.name,
        this.identity.surname,
        this.identity.email,
        this.identity.password
    );
    }

    }

    onSubmit(){
        this._UserService.updateUser(this._user).subscribe(
            response => {
                this.status = response.status;
                if(this.status != 'succes'){
                this.status = 'error'
                }else{
                localStorage.setItem('identity', JSON.stringify(this._user));
                }
            },
            error => {
            console.log(<any>error);
            }
        );
    }
}