import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task';


@Component({
    selector: 'task-new',
    templateUrl: '../views/task_new.html',
    providers: [UserService,TaskService]
})
export class TaskNewComponent implements OnInit{
    public title_page: string;
    public identity;
    public task: Task;
    public status;
    public token;

constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _taskService: TaskService
){
    this.title_page = 'Crear nueva tarea';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
        
}

   
     ngOnInit(){
        
        
        if(this.identity == null || !this.identity.sub){
            this._router.navigate(['/login']);
        }else{
            this.task =new Task(1, '', '', 'new', 'null', 'null');
        }
      
        
    }

    onSubmit(){
        this._taskService.create(this.token,this.task).subscribe(
                response=>{
                this.status = response.status;
                if(response.status != 'success'){
                    this.status='error'
                    }
                },
                error=>{
                    console.log(<any>error);
                    }
                );

    }
 
}