import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task';


@Component({
    selector: 'task-edit',
    templateUrl: '../views/task_edit.html',
    providers: [UserService,TaskService]
})
export class TaskEditComponent implements OnInit{
    public title_page: string;
    public identity;
    public task:Task;
    public status;
    public token;
    public loading;
    public id;

constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _taskService: TaskService
){
    this.title_page = 'Modificar la tarea';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
}

    ngOnInit(){
        if(this.identity && this.identity.sub ){
        this.getTask();
        }else{
            this._router.navigate(['/login']);
        }  
    }

    onSubmit(){
        
         
            this._taskService.update(this.token,this.task,this.id).subscribe(
                    response=>{
                    this.status = response.status;
                    if(response.status == 'success'){
                        
                        console.log("antes de router");
                        this._router.navigate(['/task/' + response.data.id ]);
                        }
                    },
                    error=>{
                        console.log(<any>error);
                        }
                    )
                
    }

    getTask(){
        this.loading='show';
        this._route.params.forEach((params: Params) => {
            let id = + params['id'];
            this.id = id;
            this._taskService.getTask(this.token,id).subscribe(
                response =>{
                    if(response.status =='succes'){
                        console.log(this.identity.role);
                        if(response.data.user.id == this.identity.sub || this.identity.role == "admin"){
                            this.task = response.data;
                            this.loading = 'hide';  
                        }else{
                            this._router.navigate(['/']);
                        }
                    }else{
                    this._router.navigate(['/login']);
                    }
                },
                error => {
                console.log(<any>error);
                }
            );
        });
    }
 
}
